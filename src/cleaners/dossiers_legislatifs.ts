import assert from "assert"

import { cleanActeLegislatif } from "./actes_legislatifs"
import { cleanRapporteur } from "./acteurs"
import { cleanXmlArtefacts } from "./xml"

export function cleanDossierParlementaire(dossierParlementaire: any): void {
  cleanXmlArtefacts(dossierParlementaire)

  const xsiType = dossierParlementaire["@xsi:type"]
  if (xsiType !== undefined) {
    dossierParlementaire.xsiType = xsiType
    delete dossierParlementaire["@xsi:type"]
  }

  const titreDossier = dossierParlementaire.titreDossier
  cleanXmlArtefacts(titreDossier)

  const initiateur = dossierParlementaire.initiateur
  if (initiateur !== undefined) {
    let acteurs = initiateur.acteurs
    if (acteurs !== undefined) {
      acteurs = acteurs.acteur
      if (!Array.isArray(acteurs)) {
        assert(acteurs)
        acteurs = [acteurs]
      }
      if (acteurs.length > 0) {
       initiateur.acteurs = acteurs
      } else {
        delete initiateur.acteurs
      }
    }

    let organeRef = initiateur.organes
    if (organeRef !== undefined) {
      organeRef = organeRef.organe.organeRef.uid
      assert.notStrictEqual(organeRef, undefined)
      initiateur.organeRef = organeRef
      delete initiateur.organes
    }

    if (initiateur.acteurs === undefined && initiateur.organeRef === undefined) {
      delete dossierParlementaire.initiateur
    }
  }

  let actesLegislatifs = dossierParlementaire.actesLegislatifs
  assert(actesLegislatifs)
  {
    actesLegislatifs = actesLegislatifs.acteLegislatif
    if (!Array.isArray(actesLegislatifs)) {
      assert(actesLegislatifs)
      actesLegislatifs = [actesLegislatifs]
    }
    dossierParlementaire.actesLegislatifs = actesLegislatifs
    for (const acteLegislatif of actesLegislatifs) {
      cleanActeLegislatif(acteLegislatif)
    }
  }

  let plf = dossierParlementaire.PLF
  if (plf !== undefined) {
    plf = plf.EtudePLF
    assert(Array.isArray(plf))
    delete dossierParlementaire.PLF
    dossierParlementaire.plf = plf

    for (const etudePlf of plf) {
      let rapporteurs = etudePlf.rapporteur
      if (rapporteurs !== undefined) {
        if (!Array.isArray(rapporteurs)) {
          assert(rapporteurs)
          rapporteurs = [rapporteurs]
        }
        delete etudePlf.rapporteur
        etudePlf.rapporteurs = rapporteurs
        for (const rapporteur of rapporteurs) {
          cleanRapporteur(rapporteur)
        }
      }

      let missionMinefi = etudePlf.missionMinefi
      if (missionMinefi !== undefined) {
        cleanMission(missionMinefi)
      }

      etudePlf.ordreDiqs = etudePlf.ordreDIQS
      delete etudePlf.ordreDIQS
      assert(etudePlf.ordreDiqs)
    }
  }
}

function cleanMission(mission: any) {
  let missions = mission.missions
  if (missions !== undefined) {
    missions = missions.mission
    if (!Array.isArray(missions)) {
      assert(missions)
      missions = [missions]
    }
    mission.missions = missions
    for (const missionItem of missions) {
      cleanMission(missionItem)
    }
  }
}
