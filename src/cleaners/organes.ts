import assert from "assert"

import { cleanXmlArtefacts } from "./xml"

export function cleanOrgane(organe: any): void {
  cleanXmlArtefacts(organe)

  const xsiType = organe["@xsi:type"]
  if (xsiType !== undefined) {
    organe.xsiType = xsiType
    delete organe["@xsi:type"]
  }

  const viMoDe = organe.viMoDe
  cleanXmlArtefacts(viMoDe)

  const organeParentRef = organe.organeParent
  if (organeParentRef !== undefined) {
    delete organe.organeParent
    organe.organeParentRef = organeParentRef
  }

  assert.strictEqual(organe.chambre, undefined)

  const secretariat = organe.secretariat
  if (secretariat !== undefined) {
    cleanXmlArtefacts(secretariat)
  }

  const couleurAssociee = organe.couleurAssociee
  if (couleurAssociee === null) {
    delete organe.couleurAssociee
  }

  let listePays = organe.listePays
  if (listePays !== undefined) {
    listePays = listePays.paysRef
    if (!Array.isArray(listePays)) {
      assert.strictEqual(typeof listePays, "string")
      listePays = [listePays]
    }
    organe.listePays = listePays
  }
}
