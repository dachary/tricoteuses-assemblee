import { JSDOM } from "jsdom"

import { InfosPageDepute } from "../types/deputes"

export function parsePageDepute(_pageUrl: string, html: string): InfosPageDepute {
  const { window } = new JSDOM(html)
  const { document } = window
  const emails = [...document.getElementsByTagName("a")]
    .filter(aElement => aElement.getAttribute("class") === "email")
    .filter(aElement => {
      const href = aElement.getAttribute("href")
      return href !== null && href.startsWith("mailto:")
    })
    .map(aEmailElement =>
      (aEmailElement.getAttribute("href") as string).trim().substring("mailto:".length),
    )
    .map(email => email.trim())
  const pagesFacebook = [...document.getElementsByTagName("a")]
    .filter(aElement => {
      const classAttribute = aElement.getAttribute("class")
      if (classAttribute === null) {
        return false
      }
      return classAttribute.split(" ").includes("facebook")
    })
    .filter(aElement => {
      const href = aElement.getAttribute("href")
      return href !== null
    })
    .map(aEmailElement => aEmailElement.getAttribute("href") as string)
  const pagesTwitter = [...document.getElementsByTagName("a")]
    .filter(aElement => {
      const classAttribute = aElement.getAttribute("class")
      if (classAttribute === null) {
        return false
      }
      return classAttribute.split(" ").includes("twitter")
    })
    .filter(aElement => {
      const href = aElement.getAttribute("href")
      return href !== null
    })
    .map(aEmailElement => aEmailElement.getAttribute("href") as string)
  const sitesWeb = [...document.getElementsByTagName("a")]
    .filter(aElement => aElement.getAttribute("class") === "url")
    .filter(aElement => {
      const href = aElement.getAttribute("href")
      return href !== null
    })
    .map(aEmailElement => aEmailElement.getAttribute("href") as string)
  window.close() // Free memory.

  const infos: InfosPageDepute = {
    emails: [...new Set(emails)],
    pageFacebook: pagesFacebook[0],
    pageTwitter: pagesTwitter[0],
    siteWeb: sitesWeb[0],
  }
  Object.keys(infos).forEach((key: string) => {
    if ((infos as any)[key] == undefined) {
      delete (infos as any)[key]
    }
  })
  return infos
}
