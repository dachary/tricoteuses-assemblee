/** Load all split open data files in RAM as a test. */

import commandLineArgs from "command-line-args"

import { EnabledDatasets } from "../datasets"
import { loadAssembleeData } from "../loaders"
import { Legislature } from "../types/legislatures"

const optionsDefinitions = [
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

loadAssembleeData(options.dataDir, EnabledDatasets.All, Legislature.All, {
  log: true,
})
