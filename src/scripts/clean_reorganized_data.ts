import { execSync } from "child_process"
import commandLineArgs from "command-line-args"
import fs from "fs-extra"
import path from "path"

import { cleanActeur } from "../cleaners/acteurs"
import { cleanAmendement } from "../cleaners/amendements"
import { cleanDocumentOrDivision } from "../cleaners/documents"
import { cleanDossierParlementaire } from "../cleaners/dossiers_legislatifs"
import { cleanOrgane } from "../cleaners/organes"
import { cleanReunion } from "../cleaners/reunions"
import { cleanScrutin } from "../cleaners/scrutins"
import { datasets, EnabledDatasets } from "../datasets"
import { existingDateToJson, patchedDateToJson } from "../dates"
import { walkDir } from "../file_systems"
import {
  Acteur,
  Convert as ActeursEtOrganesConvert,
  Organe,
} from "../types/acteurs_et_organes"
import { Convert as AgendasConvert, Reunion } from "../types/agendas"
import { Amendement, Convert as AmendementsConvert } from "../types/amendements"
import {
  Convert as DossiersLegislatifsConvert,
  Document,
  DossierParlementaire,
} from "../types/dossiers_legislatifs"
import { Convert as ScrutinsConvert, Scrutin } from "../types/scrutins"

const optionsDefinitions = [
  {
    alias: "c",
    help: "commit split files",
    name: "commit",
    type: Boolean,
  },
  {
    alias: "r",
    help: "push commit to given remote",
    multiple: true,
    name: "remote",
    type: String,
  },
  {
    alias: "s",
    help: "don't log anything",
    name: "silent",
    type: Boolean,
  },
  {
    alias: "v",
    help: "verbose logs",
    name: "verbose",
    type: Boolean,
  },
  {
    defaultOption: true,
    help: "directory containing Assemblée open data files",
    name: "dataDir",
    type: String,
  },
]
const options = commandLineArgs(optionsDefinitions)

function cleanReorganizedData(dataDir: string, enabledDatasets: EnabledDatasets): void {
  Date.prototype.toJSON = patchedDateToJson

  if (enabledDatasets & EnabledDatasets.ActeursEtOrganes) {
    for (const dataset of datasets.acteursEtOrganes) {
      const datasetRawDir: string = path.join(
        dataDir,
        path.basename(dataset.filename, ".json"),
      )
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      if (!options.silent) {
        console.log(`Cleaning directory: ${datasetCleanDir}…`)
      }
      fs.ensureDirSync(datasetCleanDir)
      for (const filename of fs.readdirSync(datasetCleanDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetCleanDir, filename))
      }

      for (const acteurSplitPath of walkDir(datasetRawDir, ["acteurs"])) {
        const acteurFilename = acteurSplitPath[acteurSplitPath.length - 1]
        if (!acteurFilename.endsWith(".json")) {
          continue
        }
        const acteurRawFilePath = path.join(datasetRawDir, ...acteurSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${acteurRawFilePath}…`)
        }
        const acteurRawJson: string = fs.readFileSync(acteurRawFilePath, {
          encoding: "utf8",
        })
        const acteur = JSON.parse(acteurRawJson)
        cleanActeur(acteur)
        const acteurJson: string = JSON.stringify(acteur, null, 2)

        // Validate generated JSON.
        const acteurClean: Acteur = ActeursEtOrganesConvert.toActeur(acteurJson)
        const acteurCleanJson: string = ActeursEtOrganesConvert.acteurToJson(acteurClean)

        const acteurCleanFilePath: string = path.join(datasetCleanDir, ...acteurSplitPath)
        fs.ensureDirSync(path.dirname(acteurCleanFilePath))
        fs.writeFileSync(acteurCleanFilePath, acteurCleanJson, {
          encoding: "utf8",
        })
      }

      for (const organeSplitPath of walkDir(datasetRawDir, ["organes"])) {
        const organeFilename = organeSplitPath[organeSplitPath.length - 1]
        if (!organeFilename.endsWith(".json")) {
          continue
        }
        const organeRawFilePath = path.join(datasetRawDir, ...organeSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${organeRawFilePath}…`)
        }
        const organeRawJson: string = fs.readFileSync(organeRawFilePath, {
          encoding: "utf8",
        })
        const organe = JSON.parse(organeRawJson)
        cleanOrgane(organe)
        const organeJson: string = JSON.stringify(organe, null, 2)

        // Validate generated JSON.
        const organeClean: Organe = ActeursEtOrganesConvert.toOrgane(organeJson)
        const organeCleanJson: string = ActeursEtOrganesConvert.organeToJson(organeClean)

        const organeCleanFilePath: string = path.join(datasetCleanDir, ...organeSplitPath)
        fs.ensureDirSync(path.dirname(organeCleanFilePath))
        fs.writeFileSync(organeCleanFilePath, organeCleanJson, {
          encoding: "utf8",
        })
      }

      commitAndPush(datasetCleanDir, options.commit, options.remote)
    }
  }

  if (enabledDatasets & EnabledDatasets.Agendas) {
    for (const dataset of datasets.agendas) {
      const datasetRawDir: string = path.join(
        dataDir,
        path.basename(dataset.filename, ".json"),
      )
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      if (!options.silent) {
        console.log(`Cleaning directory: ${datasetCleanDir}…`)
      }
      fs.ensureDirSync(datasetCleanDir)
      for (const filename of fs.readdirSync(datasetCleanDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetCleanDir, filename))
      }

      for (const reunionSplitPath of walkDir(datasetRawDir)) {
        const reunionFilename = reunionSplitPath[reunionSplitPath.length - 1]
        if (!reunionFilename.endsWith(".json")) {
          continue
        }
        const reunionRawFilePath = path.join(datasetRawDir, ...reunionSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${reunionRawFilePath}…`)
        }
        const reunionRawJson: string = fs.readFileSync(reunionRawFilePath, {
          encoding: "utf8",
        })
        const reunion = JSON.parse(reunionRawJson)
        cleanReunion(reunion)
        const reunionJson: string = JSON.stringify(reunion, null, 2)

        // Validate generated JSON.
        const reunionClean: Reunion = AgendasConvert.toReunion(reunionJson)
        const reunionCleanJson: string = AgendasConvert.reunionToJson(reunionClean)

        const reunionCleanFilePath: string = path.join(
          datasetCleanDir,
          ...reunionSplitPath,
        )
        fs.ensureDirSync(path.dirname(reunionCleanFilePath))
        fs.writeFileSync(reunionCleanFilePath, reunionCleanJson, {
          encoding: "utf8",
        })
      }

      commitAndPush(datasetCleanDir, options.commit, options.remote)
    }
  }

  if (enabledDatasets & EnabledDatasets.Amendements) {
    for (const dataset of datasets.amendements) {
      const datasetRawDir: string = path.join(
        dataDir,
        path.basename(dataset.filename, ".json"),
      )
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      if (!options.silent) {
        console.log(`Cleaning directory: ${datasetCleanDir}…`)
      }
      fs.ensureDirSync(datasetCleanDir)
      for (const filename of fs.readdirSync(datasetCleanDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetCleanDir, filename))
      }

      for (const amendementSplitPath of walkDir(datasetRawDir)) {
        const amendementFilename = amendementSplitPath[amendementSplitPath.length - 1]
        if (!amendementFilename.endsWith(".json")) {
          continue
        }
        const amendementRawFilePath = path.join(datasetRawDir, ...amendementSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${amendementRawFilePath}…`)
        }
        const amendementRawJson: string = fs.readFileSync(amendementRawFilePath, {
          encoding: "utf8",
        })
        const amendement = JSON.parse(amendementRawJson)
        cleanAmendement(amendement)
        const amendementJson: string = JSON.stringify(amendement, null, 2)

        // Validate generated JSON.
        const amendementClean: Amendement = AmendementsConvert.toAmendement(
          amendementJson,
        )
        const amendementCleanJson: string = AmendementsConvert.amendementToJson(
          amendementClean,
        )

        const amendementCleanFilePath: string = path.join(
          datasetCleanDir,
          ...amendementSplitPath,
        )
        fs.ensureDirSync(path.dirname(amendementCleanFilePath))
        fs.writeFileSync(amendementCleanFilePath, amendementCleanJson, {
          encoding: "utf8",
        })
      }

      commitAndPush(datasetCleanDir, options.commit, options.remote)
    }
  }

  if (enabledDatasets & EnabledDatasets.DossiersLegislatifs) {
    for (const dataset of datasets.dossiersLegislatifs) {
      const datasetRawDir: string = path.join(
        dataDir,
        path.basename(dataset.filename, ".json"),
      )
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      if (!options.silent) {
        console.log(`Cleaning directory: ${datasetCleanDir}…`)
      }
      fs.ensureDirSync(datasetCleanDir)
      for (const filename of fs.readdirSync(datasetCleanDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetCleanDir, filename))
      }

      for (const documentSplitPath of walkDir(datasetRawDir, ["documents"])) {
        const documentFilename = documentSplitPath[documentSplitPath.length - 1]
        if (!documentFilename.endsWith(".json")) {
          continue
        }
        const documentRawFilePath = path.join(datasetRawDir, ...documentSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${documentRawFilePath}…`)
        }
        const documentRawJson: string = fs.readFileSync(documentRawFilePath, {
          encoding: "utf8",
        })
        const document = JSON.parse(documentRawJson)
        cleanDocumentOrDivision(document)
        const documentJson: string = JSON.stringify(document, null, 2)

        // Validate generated JSON.
        const documentClean: Document = DossiersLegislatifsConvert.toDocument(
          documentJson,
        )
        const documentCleanJson: string = DossiersLegislatifsConvert.documentToJson(
          documentClean,
        )

        const documentCleanFilePath: string = path.join(
          datasetCleanDir,
          ...documentSplitPath,
        )
        fs.ensureDirSync(path.dirname(documentCleanFilePath))
        fs.writeFileSync(documentCleanFilePath, documentCleanJson, {
          encoding: "utf8",
        })
      }

      for (const dossierSplitPath of walkDir(datasetRawDir, ["dossiers"])) {
        const dossierFilename = dossierSplitPath[dossierSplitPath.length - 1]
        if (!dossierFilename.endsWith(".json")) {
          continue
        }
        const dossierRawFilePath = path.join(datasetRawDir, ...dossierSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${dossierRawFilePath}…`)
        }
        const dossierRawJson: string = fs.readFileSync(dossierRawFilePath, {
          encoding: "utf8",
        })
        const dossier = JSON.parse(dossierRawJson)
        cleanDossierParlementaire(dossier)
        const dossierJson: string = JSON.stringify(dossier, null, 2)

        // Validate generated JSON.
        const dossierClean: DossierParlementaire = DossiersLegislatifsConvert.toDossierParlementaire(
          dossierJson,
        )
        const dossierCleanJson: string = DossiersLegislatifsConvert.dossierParlementaireToJson(
          dossierClean,
        )

        const dossierCleanFilePath: string = path.join(
          datasetCleanDir,
          ...dossierSplitPath,
        )
        fs.ensureDirSync(path.dirname(dossierCleanFilePath))
        fs.writeFileSync(dossierCleanFilePath, dossierCleanJson, {
          encoding: "utf8",
        })
      }

      commitAndPush(datasetCleanDir, options.commit, options.remote)
    }
  }

  if (enabledDatasets & EnabledDatasets.Scrutins) {
    for (const dataset of datasets.scrutins) {
      const datasetRawDir: string = path.join(
        dataDir,
        path.basename(dataset.filename, ".json"),
      )
      const datasetCleanDir: string = datasetRawDir + "_nettoye"
      if (!options.silent) {
        console.log(`Cleaning directory: ${datasetCleanDir}…`)
      }
      fs.ensureDirSync(datasetCleanDir)
      for (const filename of fs.readdirSync(datasetCleanDir)) {
        if (filename[0] === ".") {
          continue
        }
        fs.removeSync(path.join(datasetCleanDir, filename))
      }

      for (const scrutinSplitPath of walkDir(datasetRawDir)) {
        const scrutinFilename = scrutinSplitPath[scrutinSplitPath.length - 1]
        if (!scrutinFilename.endsWith(".json")) {
          continue
        }
        const scrutinRawFilePath = path.join(datasetRawDir, ...scrutinSplitPath)
        if (options.verbose) {
          console.log(`  Cleaning file: ${scrutinRawFilePath}…`)
        }
        const scrutinRawJson: string = fs.readFileSync(scrutinRawFilePath, {
          encoding: "utf8",
        })
        const scrutin = JSON.parse(scrutinRawJson)
        cleanScrutin(scrutin)
        const scrutinJson: string = JSON.stringify(scrutin, null, 2)

        // Validate generated JSON.
        const scrutinClean: Scrutin = ScrutinsConvert.toScrutin(scrutinJson)
        const scrutinCleanJson: string = ScrutinsConvert.scrutinToJson(scrutinClean)

        const scrutinCleanFilePath: string = path.join(
          datasetCleanDir,
          ...scrutinSplitPath,
        )
        fs.ensureDirSync(path.dirname(scrutinCleanFilePath))
        fs.writeFileSync(scrutinCleanFilePath, scrutinCleanJson, {
          encoding: "utf8",
        })
      }

      commitAndPush(datasetCleanDir, options.commit, options.remote)
    }
  }

  // Restore standard conversion of dates to JSON.
  Date.prototype.toJSON = existingDateToJson
}

function commitAndPush(repositoryDir: string, commit: boolean, remotes?: string[]): void {
  if (commit) {
    execSync("git add .", {
      cwd: repositoryDir,
      env: process.env,
      encoding: "utf-8",
      stdio: ["ignore", "ignore", "pipe"],
    })
    try {
      execSync('git commit -m "Nouveautés du jour"', {
        cwd: repositoryDir,
        env: process.env,
        encoding: "utf-8",
      })
    } catch (childProcess) {
      if (
        childProcess.stderr === null ||
        !/nothing to commit/.test(childProcess.stdout)
      ) {
        console.error(childProcess.output)
        throw childProcess
      }
    }
    for (const remote of remotes || []) {
      try {
        execSync(`git push ${remote} master`, {
          cwd: repositoryDir,
          env: process.env,
          encoding: "utf-8",
          stdio: ["ignore", "ignore", "pipe"],
        })
      } catch (childProcess) {
        // Don't stop when push fails.
        console.error(childProcess.output)
      }
    }
  }
}

cleanReorganizedData(options.dataDir, EnabledDatasets.All)
